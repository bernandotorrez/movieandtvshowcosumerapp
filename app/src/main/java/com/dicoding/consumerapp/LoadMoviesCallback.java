package com.dicoding.consumerapp;

import com.dicoding.consumerapp.favourite.movies.FavouritesMovieItems;

import java.util.ArrayList;

public interface LoadMoviesCallback {
    void preExecute();
    void postExecute(ArrayList<FavouritesMovieItems> movie);
    //void postExecute(Cursor favMovie);
}
