package com.dicoding.consumerapp;

import com.dicoding.consumerapp.favourite.tvshows.FavouritesTVShowsItems;

import java.util.ArrayList;

public interface LoadTVShowsCallback {
    void preExecute();
    void postExecute(ArrayList<FavouritesTVShowsItems> tvshow);
}
